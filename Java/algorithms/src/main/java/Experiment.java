import selected.Algorithm;
import selected.genetic.GeneticAlgorithm;
import selected.pso.PSOAlgorithm;

public class Experiment {
    public static void main(String[] args) {
        System.out.println("hello world");
        runAlgorithms();
    }

    public static void runAlgorithms() {
        Algorithm pso = new PSOAlgorithm();
        var resultPSO = pso.execute();

        Algorithm ga  = new GeneticAlgorithm();
        var resultGA = ga.execute();
    }
}
