package selected;

import java.util.List;

public interface Algorithm {
    List<Integer> execute();
}
