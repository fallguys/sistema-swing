/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import utils.MethodsUtil;

/**
 *
 * @author UsuarioA
 */
public class Particle {
    
    /**
     * @param objectiveFunction to evaluate
     */
    public void evaluate(){
       Double fitness = MethodsUtil.objective_function(position);
       if (fitness > this.fitness){
           this.bestPosition = this.position;
           this.fitness = fitness;
       }
    }
    
    public void updateVelocity(ArrayList<Integer> bestPosition){
        ArrayList<Double> q1 = new ArrayList<Double>();
        ArrayList<Double> q2 = new ArrayList<Double>(); 
        for (int i=0; i < getNumDimensiones(); i++){
            Random rand1 = new Random();
            q1.add(rand1.nextDouble());
        }
        
        for (int i=0; i < getNumDimensiones(); i++){
            Random rand1 = new Random();
            q2.add(rand1.nextDouble());
        }
        
        for (int i = 0; i < this.getNumDimensiones(); i++){
            if (q1.get(i) >= q2.get(i))
                this.velocity.set(i,(bestPosition.get(i)-this.getPosition().get(i)));
            else
                this.velocity.set(i,(this.getBestPosition().get(i)-this.getPosition().get(i)));
            
        }
    }
    
    public void updatePosition(){
        for (int i = 0; i < getNumDimensiones(); i++){         
            this.position.set(i,this.position.get(i)+ new Random().nextInt(1)*this.velocity.get(i));
        }
            
    }
    /**
     * @return the numDimensiones
     */
    public Integer getNumDimensiones() {
        return numDimensiones;
    }

    /**
     * @param numDimensiones the numDimensiones to set
     */
    public void setNumDimensiones(Integer numDimensiones) {
        this.numDimensiones = numDimensiones;
    }

    /**
     * @return the position
     */
    public ArrayList<Integer> getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(ArrayList<Integer> position) {
        this.position = position;
    }

    /**
     * @return the velocity
     */
    public ArrayList<Integer> getVelocity() {
        return velocity;
    }

    /**
     * @param velocity the velocity to set
     */
    public void setVelocity(ArrayList<Integer> velocity) {
        this.velocity = velocity;
    }

    /**
     * @return the bestPosition
     */
    public ArrayList<Integer> getBestPosition() {
        return bestPosition;
    }

    /**
     * @param bestPosition the bestPosition to set
     */
    public void setBestPosition(ArrayList<Integer> bestPosition) {
        this.bestPosition = bestPosition;
    }

    /**
     * @return the fitness
     */
    public Double getFitness() {
        return fitness;
    }

    /**
     * @param fitness the fitness to set
     */
    public void setFitness(Double fitness) {
        this.fitness = fitness;
    }
    private Integer numDimensiones;
    private ArrayList<Integer> position;
    private ArrayList<Integer> velocity;
    private ArrayList<Integer> bestPosition;
    private Double fitness = -1.0;

    public Particle(){
        
    }
    public Particle(Integer numDimensiones) {
        this.numDimensiones = numDimensiones;
        this.position = new ArrayList<Integer>(Collections.nCopies(numDimensiones,0));
        this.velocity = new ArrayList<Integer>(Collections.nCopies(numDimensiones,0));
        this.bestPosition = new ArrayList<Integer>(Collections.nCopies(numDimensiones,0));
    }
    
    public Particle(Integer numDimensiones, ArrayList<Integer> position) {
        this.numDimensiones = numDimensiones;
        this.position = new ArrayList<Integer>();
        this.velocity = new ArrayList<Integer>(Collections.nCopies(numDimensiones,0));
        this.bestPosition = new ArrayList<Integer>(Collections.nCopies(numDimensiones,0));
        
        this.position = position;
    }
    
}
