/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import joinery.DataFrame;
import utils.Constants;

/**
 *
 * @author UsuarioA
 */
public class PSOAlgorithm {
    private ArrayList<Particle> particles = new ArrayList<Particle>();
    private Integer numDimensions;
    private Integer numIterations;
    private Integer numParticles;
    private Particle bestParticle;
    private Double bestFitness = -1.0;
    
    public PSOAlgorithm(Integer numDimensions,Integer numIterations,Integer numParticles){
        this.numDimensions = numDimensions;
        this.numIterations = numIterations;
        this.numParticles = numParticles;
        this.bestParticle = new Particle(numDimensions, new ArrayList<Integer>(Collections.nCopies(numDimensions, 0)));
    }
    
    
    public Particle generateParticle(){
        ArrayList<Integer> position = new ArrayList<Integer>();
        for (int i = 0; i < this.numDimensions; i++){
            Integer coordX = new Random().nextInt(Constants.SPACES_TABLES.length());
            position.add(coordX);   
        }
        
        return new Particle(this.numDimensions, position);
    }
    
    public void initializeParticles(){
        ArrayList<Particle> auxParticles = new ArrayList<>();
        for (int i =0; i < this.numParticles; i++)
            auxParticles.add(generateParticle());
        particles = auxParticles;
    }
    
    public void evaluateParticle(){
        for (Particle particle: particles){
            particle.evaluate();
            if (particle.getFitness() > this.bestFitness){
                this.bestParticle = particle;
                this.bestFitness = particle.getFitness();
            }
        }
    }
    
    public void updateParticle(){
        for (Particle particle: particles){
            particle.updateVelocity(this.bestParticle.getPosition());
            particle.updatePosition();
        }
    }
    
    public ArrayList<Integer> execute(){
        initializeParticles();
        for (int i = 0; i < this.numIterations; i++){
            evaluateParticle();
            updateParticle();
            System.out.println("ITERACION {"+(i+1)+"} =====\n");
            System.out.println("Best Fitness: {"+this.bestParticle.getFitness()+"}\n");
        }
        
        return this.bestParticle.getPosition();
    }
}
