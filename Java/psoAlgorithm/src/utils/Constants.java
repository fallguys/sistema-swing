/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import joinery.DataFrame;

/**
 *
 * @author UsuarioA
 */
public class Constants {
    public static final Integer UBIGEE = 150132;
    public static final String URL_TURNS_WITH_CAPACITY = "../../Python/data/data_turnos_con_capacidad_5k.csv";
    public static final String URL_BENEFICIARES_WITH_POINTS= "../../Python/data/muestras/data_muestras_5k_000.csv";
    public static final DataFrame SPACES_TABLES = MethodsUtil.turnosConCapacidad();
    public static final Integer PRIORITY_WEIGHT  = 2;
    public static final Integer AGGLOMERATION_WEIGHT  = 5;
    public static final Double MIN_CAPACITY_CONSTRAINT  = 0.7;
    public static final Double MAX_CAPACITY_CONSTRAINT  = 1.0;
    public static final Integer TENDENCY_TO_MAX   = 1;
    public static final Integer TENDENCY_TO_MIN   = 1;
    
}
