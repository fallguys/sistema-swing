/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import joinery.DataFrame;

/**
 *
 * @author UsuarioA
 */
public class MethodsUtil {
    
    public static Double objective_function(ArrayList<Integer> solution){
        ArrayList<Integer> eMayus = new ArrayList<Integer>(Collections.nCopies(Constants.SPACES_TABLES.length(),0));
        Double priority = 0.0;
        Double agglomeration = 0.0;
        for (int i=0; i<solution.size();i++){
            Integer e = solution.get(i);
            Integer k = Integer.parseInt(Constants.SPACES_TABLES.col("TURNOS").get(e).toString());
            Integer n = Integer.parseInt(Constants.SPACES_TABLES.col("DIA").get(e).toString());
            priority+= (1*Factors.DAY_WEIGHT)/((n+1)*(1+i))+(1*Factors.TURN_WEIGHT)/((k+1)*(1+i));
            eMayus.set(e, eMayus.get(e)+1);
        }
        
        for (int i = 0; i < Constants.SPACES_TABLES.length(); i++){
            Integer capacity = Integer.parseInt(Constants.SPACES_TABLES.col("CAPACIDAD").get(i).toString());
            Integer auxMinVal;
            if (new Double(eMayus.get(i))>Constants.MAX_CAPACITY_CONSTRAINT*capacity) auxMinVal = 1;
            else auxMinVal = 0;
            Integer auxMaxVal;
            if (new Double(eMayus.get(i))<Constants.MIN_CAPACITY_CONSTRAINT*capacity) auxMaxVal = 1;
            else auxMaxVal = 0;
            Double minVal = (new Double(eMayus.get(i)) - Constants.MIN_CAPACITY_CONSTRAINT*capacity)*auxMinVal;
            Double maxVal = (Constants.MAX_CAPACITY_CONSTRAINT*capacity-new Double(eMayus.get(i)))*auxMaxVal;
            
            agglomeration+= new Double(Constants.TENDENCY_TO_MIN)*minVal + new Double(Constants.TENDENCY_TO_MAX)*maxVal;
        }
        
        return Math.pow(priority, Constants.PRIORITY_WEIGHT)+ agglomeration*Constants.AGGLOMERATION_WEIGHT;
    }
    
    public static DataFrame turnosConCapacidad(){
        try{
            DataFrame turnos = DataFrame.readCsv(Constants.URL_TURNS_WITH_CAPACITY);
            return turnos;
        }catch(IOException e){
            System.out.println(e);
        }
        return null;
    }
}
