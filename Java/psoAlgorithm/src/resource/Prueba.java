/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource;

import model.PSOAlgorithm;
import java.io.IOException;
import java.util.List;
import joinery.DataFrame;
import joinery.DataFrame.Predicate;
import utils.Constants;
import utils.Factors;

/**
 *
 * @author UsuarioA
 */
public class Prueba {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            DataFrame zonasContagio = DataFrame.readCsv(Constants.URL_BENEFICIARES_WITH_POINTS);
            DataFrame zonasUbigeo = zonasContagio.select(new Predicate<Object>(){
                @Override
                public Boolean apply(List values){
                    return ((Long) values.get(2)).intValue() == Constants.UBIGEE;
                }
            });
            
            PSOAlgorithm pso = new PSOAlgorithm(zonasUbigeo.length(),Factors.NUM_ITERATIONS,Factors.NUM_PARTICLES);
            for (int i = 0; i < 5; i++)
                pso.execute();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            System.out.println("HOLA");
        }
        
    }
    
}
