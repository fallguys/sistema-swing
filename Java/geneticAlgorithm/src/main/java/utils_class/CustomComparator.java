/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.Comparator;
import resource_class.IndividualTimetable;

/**
 *
 * @author UsuarioA
 */
public class CustomComparator implements Comparator<IndividualTimetable> {

    @Override
    public int compare(IndividualTimetable o1, IndividualTimetable o2) {
        if (o1.getFitness() > o2.getFitness()) return 1;
        return 0;
    }
}