/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import java.util.Collections;
import resource_class.IndividualTimetable;

/**
 *
 * @author UsuarioA
 */
public class MethodsUtil {
    public static ArrayList<IndividualTimetable> concatenateArrays(ArrayList<IndividualTimetable> aux1, ArrayList<IndividualTimetable> aux2){
        ArrayList<IndividualTimetable> completeArray = new ArrayList<>(aux1.size()+aux2.size());
        Integer aux1Size = aux1.size();
        for (int i=0; i < aux1.size();i++){
            completeArray.add(aux1.get(i));
        }
        
        for (int i=0; i < aux2.size();i++){
            completeArray.add(aux2.get(i));
        }
        
        return completeArray;
    }
    
    public static ArrayList<Integer> sortIndexes(ArrayList<Double> fitness){
        ArrayList<Integer> indexes = new ArrayList<>();
        for(int i=0;i<fitness.size();i++) indexes.add(i);
        for (int i = 0; i < fitness.size(); i++) {
            for (int j = 0; j < fitness.size()-1-i; j++) { 
                if(fitness.get(j)<fitness.get(j+1)){
                    Double temp=fitness.get(j);
                    fitness.set(j, fitness.get(j+1));
                    fitness.set(j+1,temp);
                    Integer temp2 = indexes.get(j);
                    indexes.set(j,indexes.get(j+1));
                    indexes.set(j+1,temp2);
                }
            }
        }
        return indexes;
    }
    
}