/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithm_class;

import java.io.IOException;
import java.util.ArrayList;
import resource_class.Factors;
import resource_class.ProblemConstants;
import tech.tablesaw.api.Table;
import tech.tablesaw.io.csv.CsvReadOptions;
import utils_class.Constants;


public class ObjectiveFunction {
    
    public static int objective_function (ArrayList<Integer> solution) throws IOException{
        ProblemConstants a;
        Factors factors;
        ArrayList<Integer> E = new ArrayList<>();
        int priority= 0;
        int agglomeration= 0;
        int capacity;
        int e, k, n;
        Table benef = Table.read().csv(CsvReadOptions.builder
        ("../../../../../../Python/data/data_beneficiarios_con_puntos_retiro.csv"));
        int ubigee = 150132;
        Table benef_ubigee = benef.where(benef.intColumn("UBIGEO").isEqualTo(ubigee));
        a = new ProblemConstants(ubigee,benef_ubigee.rowCount());
        a.setSpaces_tables("../../../../../../Python/data/data_turnos_con_capacidad.csv");
        factors = new Factors(2, 5, 0.7, 1, 1, 1);
        for(int i=0;i<a.getSpaces_tables().rowCount();i++){
            E.add(0);
        } 
        for(int i=0;i<solution.size();i++){
            e = solution.get(i);
            Table result = a.getSpaces_tables().rows(e);
            k = result.column("TURNOS").byteSize();
            n = result.column("DIA").byteSize();
            priority += 1/((n+1)*(1+i)) + 1/((k+1)*(1+i));
            E.set(e, E.get(e)+1);
        }
        for(int i=0;i<a.getSpaces_tables().rowCount();i++){
           Table result = a.getSpaces_tables().rows(i);
           capacity = result.column("CAPACIDAD").byteSize();
           Integer auxMinVal;
           if (new Double(E.get(i))>Constants.MAX_CAPACITY_CONSTRAINT*capacity) auxMinVal = 1;
           else auxMinVal = 0;
           Integer auxMaxVal;
           if (new Double(E.get(i))<Constants.MIN_CAPACITY_CONSTRAINT*capacity) auxMaxVal = 1;
           else auxMaxVal = 0;
           Double minVal = (new Double(E.get(i)) - Constants.MAX_CAPACITY_CONSTRAINT*capacity)*auxMinVal;
           Double maxVal = (Constants.MIN_CAPACITY_CONSTRAINT*capacity-new Double(E.get(i)))*auxMaxVal;
        }
        
        return (int) Math.pow(priority, factors.getPriority_weight()) + 
                agglomeration*factors.getAgglomeration_weight(); 
        
    }
    
    public int bool_to_int(boolean b){
        return b ? 1 :0;
    }
    
}
