/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource_class;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author gianm
 */
public class IndividualTimetable {
    
    private ArrayList<Integer> chromosome;
    private double fitness;
    
    public IndividualTimetable(){
        this.chromosome = new ArrayList<>();
    }
    
    public IndividualTimetable(ArrayList<Integer> chromosome){
        this.chromosome = chromosome;
        this.fitness = -1;
    }

    public ArrayList<Integer> getChromosome() {
        return chromosome;
    }

    public void setChromosome(ArrayList<Integer> chromosome) {
        this.chromosome = chromosome;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }
    
    public ArrayList<IndividualTimetable> crossover_onepoint(ArrayList<Integer> other){
            Random r = new Random();
            IndividualTimetable ind1;
            IndividualTimetable ind2;
            ArrayList<Integer> temp1_chromosome = new ArrayList<>();
            ArrayList<Integer> temp2_chromosome = new ArrayList<>();
            ArrayList<Integer> temp1_other = new ArrayList<>();
            ArrayList<Integer> temp2_other = new ArrayList<>();
            boolean a, b;
            ArrayList<IndividualTimetable> result = new ArrayList<>();
            int c = r.nextInt(this.getChromosome().size());
            for(int i=0;i<c;i++) temp1_chromosome.add(this.getChromosome().get(i));
            for(int i=c;i<this.getChromosome().size();i++) temp2_chromosome.add(this.getChromosome().get(i));
            for(int i=0;i<c;i++) temp1_other.add(other.get(i));
            for(int i=c;i<other.size();i++) temp2_other.add(other.get(i));
            a = temp1_chromosome.addAll(temp2_other);
            b = temp2_chromosome.addAll(temp1_other);
            ind1 = new IndividualTimetable(temp1_chromosome);
            ind2 = new IndividualTimetable(temp2_chromosome);
            result.add(ind1);
            result.add(ind2);
            return result;
    }
    
    public ArrayList<IndividualTimetable> crossover_uniform(ArrayList<Integer> other){
        ArrayList<Integer> chromosome1 = new ArrayList<>();
        ArrayList<Integer> chromosome2 = new ArrayList<>();
        Random r = new Random();
        IndividualTimetable ind1;
        IndividualTimetable ind2;
        ArrayList<IndividualTimetable> result = new ArrayList<>();
        for(int i=0;i<this.getChromosome().size();i++){
            if(r.nextDouble()<0.5){
                chromosome1.add(this.getChromosome().get(i));
                chromosome2.add(other.get(i));
            }
            else{
                chromosome1.add(other.get(i));
                chromosome2.add(this.getChromosome().get(i));
            }
        }
        ind1 = new IndividualTimetable(chromosome1);
        ind2 = new IndividualTimetable(chromosome2);
        result.add(ind1);
        result.add(ind2);
        return result;
    }   
    
    public IndividualTimetable mutate_position(){
        IndividualTimetable mutated_ind;
        int indexPos, newPos;
        Random r = new Random();
        Random s = new Random();
        mutated_ind = new IndividualTimetable(this.getChromosome());
        indexPos = r.nextInt(mutated_ind.getChromosome().size()-1);
        newPos = r.nextInt(mutated_ind.getChromosome().size()-1);
        mutated_ind.getChromosome().set(indexPos, this.getChromosome().get(newPos));
        mutated_ind.getChromosome().set(newPos, this.getChromosome().get(indexPos));
        return mutated_ind;
    }
    
    public IndividualTimetable mutate_value(){
        IndividualTimetable mutated_ind;
        int indexPos, newValue;
        mutated_ind = new IndividualTimetable(this.getChromosome());
        Random r = new Random();
        Random s = new Random();
        indexPos = r.nextInt(mutated_ind.getChromosome().size()-1);
        //newValue = s.nextInt();
        //mutated_ind.getChromosome().set(indexPos, newValue);
        return mutated_ind;
    }
}
