/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource_class;

/**
 *
 * @author gianm
 */
public class Factors {
    
    private int priority_weight;
    private int agglomeration_weight;
    private double min_capacity_constraint;
    private int max_capacity_constraint;
    private int tendency_to_max;
    private int tendency_to_min;
    
    public Factors(){
        
    }
    
    public Factors(int priority_weight, int agglomeration_weight, 
            double min_capacity_constraint, int max_capacity_constraint, 
            int tendency_to_max, int tendency_to_min){
        this.priority_weight = priority_weight;
        this.agglomeration_weight = agglomeration_weight;
        this.min_capacity_constraint = min_capacity_constraint;
        this.max_capacity_constraint = max_capacity_constraint;
        this.tendency_to_max = tendency_to_max;
        this.tendency_to_min = tendency_to_min;
    }
    

    public int getPriority_weight() {
        return priority_weight;
    }

    public void setPriority_weight(int priority_weight) {
        this.priority_weight = priority_weight;
    }

    public int getAgglomeration_weight() {
        return agglomeration_weight;
    }

    public void setAgglomeration_weight(int agglomeration_weight) {
        this.agglomeration_weight = agglomeration_weight;
    }

    public double getMin_capacity_constraint() {
        return min_capacity_constraint;
    }

    public void setMin_capacity_constraint(double min_capacity_constraint) {
        this.min_capacity_constraint = min_capacity_constraint;
    }

    public int getMax_capacity_constraint() {
        return max_capacity_constraint;
    }

    public void setMax_capacity_constraint(int max_capacity_constraint) {
        this.max_capacity_constraint = max_capacity_constraint;
    }

    public int getTendency_to_max() {
        return tendency_to_max;
    }

    public void setTendency_to_max(int tendency_to_max) {
        this.tendency_to_max = tendency_to_max;
    }

    public int getTendency_to_min() {
        return tendency_to_min;
    }

    public void setTendency_to_min(int tendency_to_min) {
        this.tendency_to_min = tendency_to_min;
    }
     
}
