/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource_class;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import tech.tablesaw.api.Table;
import tech.tablesaw.io.csv.CsvReadOptions;
import static utils_class.Constants.URL_TURNS_WITH_CAPACITY;
import utils.MethodsUtil;
import utils.CustomComparator;
import algorithm_class.ObjectiveFunction;

/**
 *
 * @author UsuarioA
 */
public class GeneticAlgorithm {
    private Integer populationSize;
    private Integer chromosomeSize;
    private Integer iterationNumber;
    private Double mutationRate;
    private ArrayList<IndividualTimetable> population;
    
    public ArrayList<IndividualTimetable> initPopulation() throws IOException{
        ArrayList<IndividualTimetable> population = new ArrayList<>();
        for (int i=0; i < populationSize; i++){
            ArrayList<Integer> chromosome = new ArrayList<>();
            for (int j=0; j < chromosomeSize; j++){
                Integer aux_chromosome = new Random().nextInt(Table.read().csv(CsvReadOptions.builder(URL_TURNS_WITH_CAPACITY)).rowCount());
                chromosome.add(aux_chromosome);
            }
                
            population.add(new IndividualTimetable(chromosome));
        }
        
        return population;
    }
    
    public ArrayList<IndividualTimetable> selectParentsRoulette(){
        Integer popsize = this.populationSize;
        ArrayList<IndividualTimetable> population = this.population;
        Integer iParent1 = 0;
        Integer iParent2 = 0;
        
        Double sumFitness = 0.0;
        for (IndividualTimetable indiv: population) sumFitness+=indiv.getFitness();
        Double pickFitness = new Random().nextDouble()*sumFitness;
        Double cumFitness = 0.0;
        for (int i = 0; i < popsize; i++){
            cumFitness += population.get(i).getFitness();
            if (cumFitness > pickFitness){
                iParent1 = i;
                break;
            }
        }
        
        sumFitness -= population.get(iParent1).getFitness();
        for (IndividualTimetable indiv: population) sumFitness+=indiv.getFitness();
        pickFitness = new Random().nextDouble()*sumFitness;
        cumFitness = 0.0;
        for (int i = 0; i < popsize; i++){           
            if (i == iParent1) continue;
            cumFitness += population.get(i).getFitness();
            if (cumFitness > pickFitness){
                iParent2 = i;
                break;
            }
        }
        ArrayList<IndividualTimetable> aux =new ArrayList<IndividualTimetable>(2);
        aux.add(population.get(iParent1));
        aux.add(population.get(iParent2));
        return aux;
    }
    
    public ArrayList<IndividualTimetable> selectSurvivors(ArrayList<IndividualTimetable> offspringPopulation, Integer numSurvivors){
        ArrayList<IndividualTimetable> population = this.population;
        ArrayList<IndividualTimetable> nextPopulation = new ArrayList<>();
        population = MethodsUtil.concatenateArrays(population, offspringPopulation);
        //ArrayList<IndividualTimetable> auxSurvivors = population;
        //Collections.sort(auxSurvivors,new CustomComparator());
        //auxSurvivors = (ArrayList) auxSurvivors.subList(0, numSurvivors);
        ArrayList<Integer> iSurvivors = new ArrayList<>();
        //for (IndividualTimetable ind: auxSurvivors){
            //iSurvivor.add(ind.)
        //}
        //for (int i = 0; i < numSurvivors; i++) 
            //nextPopulation.add(population.get(iSurvivors.get(i)));
        //return nextPopulation;
        ArrayList<Double> fitness = new ArrayList<>();
        for(int i=0;i<population.size();i++){
            fitness.add(population.get(i).getFitness());
        }
        iSurvivors = (ArrayList<Integer>) MethodsUtil.sortIndexes(fitness).subList(0, numSurvivors);
        for(int i=0;i<numSurvivors;i++){
            nextPopulation.add(population.get(iSurvivors.get(i)));
        }
        return nextPopulation;
    }
    
    public void evaluatePopulation(ArrayList<IndividualTimetable> population) throws IOException{
        Integer popSize = this.populationSize;
        
        for (int i = 0; i<popSize; i++)
            if (population.get(i).getFitness() != -1)
                population.get(i).setFitness(ObjectiveFunction.objective_function(population.get(i).getChromosome()));
    }
    
    public void execute() throws IOException{
        int popsize = this.populationSize;
        ArrayList<Integer> ibest = new ArrayList<>();
        ArrayList<Double> fitness = new ArrayList<>();
        Double bestfitness;
        this.evaluatePopulation(this.population);
        for(int i=0;i<population.size();i++){
            fitness.add(population.get(i).getFitness());
        }
        ibest = (ArrayList<Integer>) MethodsUtil.sortIndexes(fitness).subList(0,1);
        bestfitness=this.population.get(ibest.get(0)).getFitness();   
    }
    
}