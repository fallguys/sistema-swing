/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource_class;

import java.util.ArrayList;

/**
 *
 * @author gianm
 */
public class Individual {
    private int[] chromosome;
    
    public Individual(int[] chromosome){
        this.chromosome = chromosome;
    }


    public int[] getChromosome() {
        return chromosome;
    }

    public void setChromosome(int[] chromosome) {
        this.chromosome = chromosome;
    }
    
    //public crossover_onepoint(other);
    //public crossover_uniform(other);
    //public mutate_position();
    //public mutate_value();
    
}
