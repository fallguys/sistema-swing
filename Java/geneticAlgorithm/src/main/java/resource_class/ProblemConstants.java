/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource_class;

import java.io.IOException;
import tech.tablesaw.api.Table;
import tech.tablesaw.io.csv.CsvReadOptions;

/**
 *
 * @author gianm
 */
public class ProblemConstants {
    private int ubigee;
    private int num_beneficiaries;
    private Table spaces_tables;
    
    public ProblemConstants(){
        this.ubigee = 150132;
        this.num_beneficiaries = 100;
    }
    public ProblemConstants(int ubigee, int num_beneficiaries){
        this.ubigee = ubigee;
        this.num_beneficiaries = num_beneficiaries;
    }

    public int getUbigee() {
        return ubigee;
    }

    public void setUbigee(int ubigee) {
        this.ubigee = ubigee;
    }

    public int getNum_beneficiaries() {
        return num_beneficiaries;
    }

    public void setNum_beneficiaries(int num_beneficiaries) {
        this.num_beneficiaries = num_beneficiaries;
    }

    public Table getSpaces_tables() {
        return spaces_tables;
    }

    public void setSpaces_tables(String url) throws IOException {
        this.spaces_tables = Table.read().csv(CsvReadOptions.builder(url));
    }
   
}
