import numpy as np
import pandas as pd

def adding_weights(path_beneficiaries,path_weighted_beneficiaries, gender_weight, disabled_weight, age_weight):
    benef = pd.read_csv(path_beneficiaries, sep = ',', encoding = 'utf-8')
    weights = []
    try:
        for index, row in benef.iterrows():
            weight=0                      
            if row['FLAG_MAYEDAD']:
                weight+=age_weight
            if row['FLAG_DISCAP_SEVERA']:
                weight+=disabled_weight
            if row['DE_GENERO']:
                weight+=gender_weight
            weights.append(weight)
        benef.insert(12,"PESO",weights,True)
        benef = benef.sort_values(by=['PESO'], ascending=False)
        benef.to_csv(path_weighted_beneficiaries,encoding='utf-8')
        print("ARCHIVO GENERADO EXITOSAMENTE")
    except Exception as e:
        print(str(e), "ERROR AL GENERAR BENEFICIARIOS CON PESO")
        