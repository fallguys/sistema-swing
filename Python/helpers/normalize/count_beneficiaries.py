import numpy as np
from numpy import random
import pandas as pd
from math import ceil
def simulated_beneficiaries(originPath, endPath, prPath):    
    #benef Beneficiarios
    benef = pd.read_csv(originPath, sep = ';', encoding = 'latin-1')
    benef = benef.sample(frac=1)
    n = benef["FLAG_MAYEDAD"].count()
    muestra = ceil(0.05*n)
    muestra
    #GENERACIÓN DE ADULTOS MAYORES
    benefAux = benef["FLAG_MAYEDAD"]
    count_aux = 0
    visited = []
    while count_aux < muestra:
        i = random.randint(n)
        if i not in visited:
            benefAux[i] = 0
            count_aux+=1
            visited.append(i)
    #SIMULACIÓN DE LA INTENCIÓN DE PUNTO DE RECOJO
    pr = pd.read_csv(prPath, sep = ',', encoding = 'utf-8')
    prList = [0 for x in range(benef["CO_HOGAR"].count())]
    total = benef["CO_HOGAR"].count()
    muestra =ceil( 1 * total)     
    auxPR = pr.groupby('UBIGEO')
    count_a = 0
    visited = []
    try:
        for index, row in benef.iterrows():                        
            if row['UBIGEO'] in auxPR.groups.keys():
                auxPRB = auxPR.get_group(row['UBIGEO'])
                auxPRB = auxPRB['CODIGO'].tolist()
                j = random.choice(auxPRB)                
            else:
                j = -1                
            prList[count_a] = j
            count_a += 1
        benef["CODIGO_PUNTOS_RETIRO"] = prList
        benef.to_csv(endPath, encoding="utf-8")
        print("ARCHIVO GENERADO EXITOSAMENTE")
    except Exception as e:
        print(str(e),"ERROR AL GENERAR DATOS SIMULADORES")
    

def generate_capacity(covid,maxAforo,minAforo,pathIni,pathEnd,tiempo_atencion=10):    
    try:
        pr = pd.read_csv(pathIni, sep = ',', encoding = 'utf-8')  
        auxAforo = []
        auxTiempo = []        
        try:
            for x in range(len(pr)) :
                auxAforo.append(random.randint(round(minAforo*covid),round(maxAforo*covid)))
                auxTiempo.append(tiempo_atencion)
        except Exception as e:
            print(str(e))
        pr["AFORO"] = auxAforo
        pr["TIEMPO ATENCIÓN (min)"]= auxTiempo
        pr.to_csv(pathEnd, encoding="utf-8", index=False)
        print("ARCHIVO GENERADO CON ÉXITO")
    except Exception as e:
        print(str(e))
        print("ERROR EN LA GENERACIÓN DE CAPACIDADES")