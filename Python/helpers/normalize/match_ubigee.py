import numpy as np
import pandas as pd
import random as random

# CARGA DE AGENCIAS
def get_equality_prov(name):
    switcher={
        "NAZCA":"NASCA",
        "ANDRES AVELINO CACERES D.":"ANDRES AVELINO CACERES DORREGARAY",
        "LIMA REGION":"LIMA",
        "CORONEL GREGORIO ALBARRACIN L.":"CORONEL GREGORIO ALBARRACIN LANCHIPA",
        "ANCO HUALLO":"ANCO_HUALLO",
        "HUAY HUAY":"HUAY-HUAY",
        "SANTO DOMINGO DE ANDA":"SANTO DOMINGO DE ANDIA",
        "ANTONIO RAIMONDI":"ANTONIO RAYMONDI",
        "SAN PEDRO DE PUTINA PUNCU":"SAN PEDRO DE PUTINA PUNCO",
        "SAN FCO DE ASIS DE YARUSYACAN":"SAN FRANCISCO DE ASIS DE YARUSYACAN",
        "QUITO ARMA":"QUITO-ARMA",
        "SAN FRANCISCO DE YESO":"SAN FRANCISCO DEL YESO",
        "ESTIQUE PAMPA":"ESTIQUE-PAMPA"
    }
    return switcher.get(name,"NO ENCONTRADO")

def generate_ubigee(originPath, endPath, ubigeePath):
    pr = pd.read_csv(originPath, sep=';', encoding='latin-1')
    # CARGA DE UBIGEOS
    u = pd.read_csv(ubigeePath, sep=';', encoding='latin-1')

    zonas = pr[["DEPARTAMENTO", "PROVINCIA", "DISTRITO"]]
    zonas = zonas.rename(columns={"DEPARTAMENTO": "NOMBDEP", "PROVINCIA": "NOMBPROV", "DISTRITO": "NOMBDIST"})
    aux = []
    for indexZ, rowZ in zonas.iterrows():        
        if get_equality_prov(rowZ["NOMBDEP"]) != "NO ENCONTRADO":
            rowZ["NOMBDEP"] = get_equality_prov(rowZ["NOMBDEP"])
        if get_equality_prov(rowZ["NOMBPROV"]) != "NO ENCONTRADO":
            rowZ["NOMBPROV"] = get_equality_prov(rowZ["NOMBPROV"])
        if get_equality_prov(rowZ["NOMBDIST"]) != "NO ENCONTRADO":
            rowZ["NOMBDIST"] = get_equality_prov(rowZ["NOMBDIST"])        
        departamento = rowZ["NOMBDEP"]
        if rowZ["NOMBPROV"] == "EN INVESTIGACIÓN":
            try:
                auxProvincia = u.loc[u['NOMBDEP']==departamento]            
                randLimit = len(auxProvincia)
                indice = random.randint(0,randLimit - 1)                        
                provincia = (auxProvincia.iloc[indice])["NOMBPROV"]
            except:
                print("ERROR {}\nDepartamento\t\tProvincia\t\tDistrito\t\t\n{}\t\t{}\t\t{}\n".format(indexZ, departamento))
        else:
            provincia = rowZ["NOMBPROV"]                 
        if rowZ["NOMBDIST"] == "EN INVESTIGACIÓN":
            try:                
                auxDistrito = u.loc[(u['NOMBDEP']==departamento) & (u['NOMBPROV']==provincia)]                 
                randLimit = len(auxDistrito)
                indice = random.randint(0,randLimit - 1)                        
                distrito = (auxDistrito.iloc[indice])["NOMBDIST"]
            except:
                print("ERROR {}\nDepartamento\t\tProvincia\t\tDistrito\t\t\n{}\t\t{}\t\t{}\n".format(indexZ, departamento))
                      
        else:            
            distrito = rowZ["NOMBDIST"]
        try:
            val = u.loc[(u["NOMBDEP"] == departamento) & (u["NOMBPROV"] == provincia) & (u["NOMBDIST"] == distrito)]
            aux.append(val.iat[0, 0])
        except:
            print("ERROR {}\nDepartamento\t\tProvincia\t\tDistrito\t\t\n{}\t\t{}\t\t{}\n".format(indexZ, departamento,
                                                                                                 provincia, distrito))
    pr.insert(3, "Ubigeo", aux, True)
    pr = pr.rename(columns={"Ubigeo": "UBIGEO"})
    pr.to_csv(endPath, index=False, encoding="utf-8")
    print("ARCHIVO AGENCIAS UBIGEO GENERADO")
