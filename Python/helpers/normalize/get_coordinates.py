import numpy as np
import pandas as pd
import geocoder

data2 = pd.read_csv('../data/agencias.csv', sep = ';', encoding = 'latin-1')
i=0
longitud = []
latitud = []
for col in data2.values:
    i+=1
    try:
        loc = geocoder.google(col[6]+", "+col[5]+" - "+col[4], key='AIzaSyArwb2ehL1R7n4IIt7sRPDGik7cL6K55xA')
        longitud.append(loc[0].geometry['coordinates'][0])
        latitud.append(loc[0].geometry['coordinates'][1])
    except:
        print(i)
data2.insert(9,"LONGITUD",longitud,True)
data2.insert(10,"LATITUD",latitud,True)
data2 = data2.rename(columns={"HORARIO DE ATENCIÓN AL PÚBLICO\nLUNES- VIERNES": "HORARIO ATENCIÓN LUNES-VIERNES"})
data2 = data2.rename(columns={"HORARIO DE ATENCIÓN AL PÚBLICO SABADO": "HORARIO ATENCIÓN SABADO"})
data2.to_csv("../data/agencias_v1_29092020.csv")